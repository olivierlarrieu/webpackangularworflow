export default function router($stateProvider, $urlRouterProvider) {

    $stateProvider
    .state('main', {
        url: '/main',
       templateProvider: ['$q', function ($q) {
           let deferred = $q.defer();
           require.ensure(['./templates/main.html'], function () {
               let template = require('./templates/main.html');
               deferred.resolve(template);
           });
           return deferred.promise;
       }],
       controller: 'MainController',
       controllerAs: 'main',
       resolve: {
          foo: ['$q', '$ocLazyLoad', '$LoginService', '$state', '$log',
                function ($q, $ocLazyLoad, $LoginService, $state, $log) {              
               let deferred = $q.defer();
                if(!$LoginService.isAuthenticated) {
                  $log.error("not authorized")
                  deferred.reject();
                }
               require.ensure([], function () {
                   let module = require('./main.module.js');                  
                   $ocLazyLoad.load({
                       name: module
                   });
                   deferred.resolve(module);
               });
               return deferred.promise;
           }]
       }
    })
    .state('main.layout', {
        url: '/myBackoffice',
        views: {
        "menu":{
         templateProvider: ['$q', function ($q) {
             let deferred = $q.defer();
             require.ensure(['./templates/layout/menu/menu.html'], function () {
                 let template = require('./templates/layout/menu/menu.html');
                 deferred.resolve(template);
             });
             return deferred.promise;
         }]
       },
        "content":{
         templateProvider: ['$q', function ($q) {
             let deferred = $q.defer();
             require.ensure(['./templates/layout/content.html'], function () {
                 let template = require('./templates/layout/content.html');
                 deferred.resolve(template);
             });
             return deferred.promise;
         }]
       },
        "footer":{
         templateProvider: ['$q', function ($q) {
             let deferred = $q.defer();
             require.ensure(['./templates/layout/footer.html'], function () {
                 let template = require('./templates/layout/footer.html');
                 deferred.resolve(template);
             });
             return deferred.promise;
         }]
       }}
    })
};
