export default function router($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/login/form");
    $stateProvider
    .state('main.layout.dashboard', {
        url: '/dashboard',
        title: 'Dashboard',
        isDropdown: true,
        dropDownElems: [{title: "Home", uiSref: "main.layout.dashboard"}],
         templateProvider: ['$q', function ($q) {
             let deferred = $q.defer();
             require.ensure(['./templates/dashboard.html'], function () {
                 let template = require('./templates/dashboard.html');
                 deferred.resolve(template);
             });
             return deferred.promise;
         }],
       controller: 'DashboardController',
       controllerAs: 'dashboard',
       resolve: {
          foo: ['$q', '$ocLazyLoad', '$LoginService', '$state', '$log',
                function ($q, $ocLazyLoad, $LoginService, $state, $log) {              
               let deferred = $q.defer();
                if(!$LoginService.isAuthenticated) {
                  $log.error("not authorized")
                  deferred.reject();
                }
               require.ensure([], function () {
                   let module = require('./dashboard.module.js');                  
                   $ocLazyLoad.load({
                       name: module
                   });
                   deferred.resolve(module);
               });
               return deferred.promise;
           }]
       }

    })
};
