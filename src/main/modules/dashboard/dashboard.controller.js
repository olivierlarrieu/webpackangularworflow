export class DashboardController {
  constructor($http, $state, $configs, $LoggedUser) {
    this.$http = $http;
    this.$state = $state;
    this.$configs = $configs;
    this.$LoggedUser = $LoggedUser;
    this.name = 'DashboardController';
  }
}

DashboardController.$inject = ['$http', '$state', '$configs', '$LoggedUser'];
