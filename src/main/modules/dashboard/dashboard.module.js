import {DashboardController} from './dashboard.controller.js';

export default angular.module('main.dashboard.module', [])
	.controller('DashboardController', DashboardController)
	.name
