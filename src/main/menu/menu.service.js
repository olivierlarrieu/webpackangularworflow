/* 
 * le service $LoggedUser est disponible dans l ensemble de l application
 * il est injecte dans le module login.module
 * son role est de contenir et fournir les informations relatives a l'utilisateur connecte
 * 
 */
export class $menu {
  constructor($http, $state) {
    this.$http = $http;
    this.$state = $state;
  }
  init()
  {
    this.items = [];
    var elem;
    var routes = this.$state.get();
    for(elem in routes) {
        if(routes[elem].title && !routes[elem].isDropdown) {
            console.log(routes[elem].title);
            this.items.push(routes[elem]);
        }
        if(routes[elem].title && routes[elem].isDropdown && routes[elem].dropDownElems) {
            console.log(routes[elem].title);
            this.items.push(routes[elem]);
        }
    }
  }
}

/* injector */
$menu.$inject = ['$http', '$state'];
