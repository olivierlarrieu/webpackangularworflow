/* 
 * le controller principal du main 
 * 
*/
export class MainController {
  constructor($http, $state, $configs, $LoginService, $LoggedUser, $menu) {
    this.$http = $http;
    this.$state = $state;
    this.$configs = $configs;
    this.$LoginService = $LoginService;
    this.$LoggedUser = $LoggedUser;
    this.$menu = $menu; 
    this.name = 'MainController';
    this.sideMenuItems = [];
    this.$menu.init();
  }

  logout()
  {
      let self = this;
      this.$LoginService.logout(
        {username:this.username, password:this.password}).then(
        function(response){
          console.log(response);
          self.$state.go('login.form');
        },
        function(error){
          console.log(error);
        })
  }
}
MainController.$inject = ['$http', '$state', '$configs', '$LoginService', '$LoggedUser', '$menu'];
