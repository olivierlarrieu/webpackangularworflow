/*
 * ce module est lazy load lors du succes de l authentification
 * il a pour responsibilite de charger les modules
 * que nous souhaitons mettre a disposition a l utilisateur
 * ici pour exemple le module dashboard qui sera integre dans le content du main.layout
 * l 'intégration d'un module se fait par son fichier route et non le module.
*/
import bootstrap from 'angular-ui-bootstrap';

import routes from './main.routes.js';
import {$menu} from './menu/menu.service.js';
import {MainController} from './main.controller.js';
import globalConfigs from '../configs/configs';

require("font-awesome-webpack");
require('./css/sb-admin.css');
window.jQuery = require('jquery/dist/jquery.js');
require('bootstrap/dist/js/bootstrap.js');

/* enable or not ui-view animations */
if(globalConfigs.useUiViewAnimations){
	/* enable ui-view animation */
	require('./css/ui-animations.css');
}

/* les modules specifiques a la partie main s injecte ici par leur fichier routes*/
import dashboardRoutes from './modules/dashboard/dashboard.routes.js';

export default angular.module('main.module', [bootstrap])
	.service('$menu', $menu)
	.controller('MainController', MainController)
    .config(['$stateProvider', '$urlRouterProvider', dashboardRoutes])
    .config(['$stateProvider', '$urlRouterProvider', routes])
    .run(function(){console.log('hello main')})
    .name
