/* configuration generale a l application */
let globalConfigs = 
{	
    siteName: "Angular and WebPack ECMA 2015 is Awesome ...", // nom de l application dans l onglet du navigateur
    useUiViewAnimations: true // si on souhaite utiliser les animations sur les ui-view
}
export default globalConfigs
