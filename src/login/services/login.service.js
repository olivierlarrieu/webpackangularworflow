/* 
 * le service $LoginService est disponible dans l ensemble de l application
 * il est injecte dans le module login.module
 * son role est de fournir les methodes relatives a l' authentification de l utilisateur
 * 
 * les methodes doivent retourner une promise et être consommer à partir d un controller
 * il n est pas bon d utiliser le service directement a partir d un template.
 * le service fournit les méthodes minima de login et de logout
 *
 */
export class $LoginService {
  constructor($q, $http, $state, loginConfigs, $LoggedUser) {
    this.$q = $q;
    this.$http = $http;
    this.$state = $state;
    this.loginConfigs = loginConfigs;
    this.$LoggedUser = $LoggedUser;
    this.init();
  }
  init()
  {
  	  this.isAuthenticated = false;
      this.$LoggedUser.init();
  }
  authenticate(credentials={username:undefined, password: undefined})
  {
      /* process server side authentification and return user datas or error */
      var self = this;
      return this.$q(function(resolve, reject) {
        setTimeout(function(){
          self.isAuthenticated = true;
          self.$LoggedUser.setDatas(
            { email: credentials.username + "@user.com",
              username: credentials.username,
              lastname: credentials.lastname,
              id: 5125
            }
          )
          credentials = undefined;
          resolve("success");
        },
        self.loginConfigs.delayBeforeMain);
      })
  }
  logout()
  {
      this.init();
      var self = this;
      return this.$q(function(resolve, reject) {
          setTimeout(function(){resolve("success");
        },
        self.loginConfigs.delayBeforeMain);
      });
  }
}

/* injector */
$LoginService.$inject = ['$q', '$http', '$state', 'loginConfigs', '$LoggedUser'];
