/* 
 * le service $LoggedUser est disponible dans l ensemble de l application
 * il est injecte dans le module login.module
 * son role est de contenir et fournir les informations relatives a l'utilisateur connecte
 * 
 */
export class $LoggedUser {
  constructor($http, $state, loginConfigs) {
    this.$http = $http;
    this.$state = $state;
    this.loginConfigs = loginConfigs;
    this.init();
    this.datas;
    let self = this;
  }
  init()
  {
  	this.datas = undefined;
  }
  get Datas()
  {
    return this.datas;
  }
  setDatas(datas)
  {
  	this.datas = datas;
  }
}

/* injector */
$LoggedUser.$inject = ['$http', '$state', 'loginConfigs'];
