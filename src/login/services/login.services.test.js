import loginModule from '../login.module';

describe('Service: LoggedUserService', function() {
  var $LoginService;

  beforeEach(angular.mock.module(loginModule.name));

  beforeEach(angular.mock.inject(function(_$LoginService_) {
    $LoginService = _$LoginService_;
    
  }));

  it('test #loggedUser.setData()', function() {
    $LoginService.$LoggedUser.setDatas({username: "larrieu", email: 'none.com'})

    expect($LoginService.$LoggedUser.Datas.username).toBe('larrieu');
    expect($LoginService.$LoggedUser.Datas.email).toBe('none.com');
  });
  it('test #authenticate()', function() {
    $LoginService.authenticate({username: "larrieu", email: 'none.com'});
    expect($LoginService.$LoggedUser.Datas.username).toBe(undefined);
  });
  it('test #logout()', function() {
    $LoginService.logout();
    expect($LoginService.$LoggedUser.Datas).toBe(undefined);
  });
});
