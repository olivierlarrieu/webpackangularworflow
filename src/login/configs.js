/* configuration du module login */
let configs = 
{	
        useSubscribe: false,       // utilisation du formulaire d inscription
        useForgetPassword: true,   // utilisation du formulaire forget password
        delayBeforeMain: 500       // delais de la redirection vers le main ui-sref="main.layout"
}
export default configs
