/*
 * le module login fournit le necessaire pour:
 *   - la page de login
 *   - les formulaires:
 *        - login
 *        - registration
 *        - forget password
 * le module est configurable via son fichier configs.js
 * le controller utilise le service $LoginService afin d'authentifier l utilisateur
 * c est le controller qui redirige vers le module main.layout
 *
*/

import uirouter from 'angular-ui-router';
import configs from './configs';
import {$LoginService} from './services/login.service.js';
import {$LoggedUser} from './services/loggedUser.service.js';
var LoginController = require('./login.controller.js');
var loginRoutes = require('./login.routes');
import globalConfigs from '../configs/configs';
/* enable or not ui-view animations */
if(globalConfigs.useUiViewAnimations){
	/* enable ui-view animation */
	require('./css/ui-animations.css');
}
//import './login.css';
require('./css/login.css');
export default angular.module('login.module', [uirouter])
	.constant('loginConfigs', configs)
	.service('$LoggedUser', $LoggedUser)
    .service('$LoginService', $LoginService)
	.controller('LoginController', LoginController)
    .config(['$stateProvider', '$urlRouterProvider', loginRoutes])
    .name
