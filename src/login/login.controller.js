
function LoginController ($http, $state, $configs, $LoginService, loginConfigs, $LoggedUser) {
    this.$http = $http;
    this.$state = $state;
    this.name = 'LoginController';
    this.$LoginService = $LoginService;
    this.$LoggedUser = $LoggedUser;
    this.loginConfigs = loginConfigs;
    this.authenticate = function (form) {
        var self = this;
        if(form.$valid){

	        this.$LoginService.authenticate(
            {username:this.username, password:this.password}).then(
            function(response){
              self.$state.go('main.layout');
            },
            function(error){
              console.error(error);
            })
        }
    }
}

/* injector */
LoginController.$inject = ['$http', '$state', '$configs', '$LoginService','loginConfigs', '$LoggedUser']

module.exports = LoginController;
