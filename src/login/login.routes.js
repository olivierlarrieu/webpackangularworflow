function router($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/login/form");
    $stateProvider
    .state('login', {
       url: '/login',
       templateProvider: ['$q', function ($q) {
           var deferred = $q.defer();
           require("../assets/angularjs.jpg")
           require.ensure(['./templates/login.html'], function () {
               var template = require('./templates/login.html');
               deferred.resolve(template);
           });
           return deferred.promise;
       }],
       controller: 'LoginController',
       controllerAs: 'login',
    })
    .state('login.form', {
       url: '/form',
       templateProvider: ['$q', function ($q) {
           var deferred = $q.defer();
           require.ensure(['./templates/form.html'], function () {
               var template = require('./templates/form.html');
               deferred.resolve(template);
           });
           return deferred.promise;
       }]
    })
    .state('login.subscribe', {
        url: '/subscribe',
       templateProvider: ['$q', function ($q) {
           var deferred = $q.defer();
           require.ensure(['./templates/subscribe.html'], function () {
               var template = require('./templates/subscribe.html');
               deferred.resolve(template);
           });
           return deferred.promise;
       }]
    })
    .state('login.forget', {
        url: '/forget',
       templateProvider: ['$q', function ($q) {
           var deferred = $q.defer();
           require.ensure(['./templates/forget.html'], function () {
               var template = require('./templates/forget.html');
               deferred.resolve(template);
           });
           return deferred.promise;
       }]
    })
};

module.exports = router;
