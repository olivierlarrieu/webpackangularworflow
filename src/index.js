/*
 * index.js est le point d entree de l application
 */

/* import angular nodes modules usefull for your app */
import angular from 'angular';
import uirouter from 'angular-ui-router';
require('ui-router-extras/release/ct-ui-router-extras.js');
import oclazyload from 'oclazyload';
import angularAnimate from 'angular-animate';
/* import less bootstrap main style */
import 'bootstrap/less/bootstrap.less';
/* import the app components */
import globalConfigs from './configs/configs';
import login from './login/login.module';
import ngLoader from './common-services/ng-loaders/ngLoader.js';
import routes from './main/main.routes';
/* initialize the main module of your app */
angular.module(
	'myapp',
	[	
		oclazyload,
		uirouter,
		'ct.ui.router.extras',
		angularAnimate,
		ngLoader,
		login
	]
)
.constant('$configs', globalConfigs)
.config(['$stateProvider', '$urlRouterProvider', routes])
